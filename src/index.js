const express = require('express')
const fs = require('fs')
const path = require('path')
const { promisify } = require('util');
const {STATIC} = require('./paths')
const app = express()
const port = process.env.PORT || 3000

const readDir = promisify(fs.readdir);
const readFile = promisify(fs.readFile);
const getStats = promisify(fs.stat);

const getFiles = async () => {
    const filenames = await readDir(STATIC);
    const examData = filenames.map(file =>
        readFile(path.join(STATIC, file))
    );
    const statsData = filenames.map(file =>
        getStats(path.join(STATIC, file))
    );
    const resolved = await Promise.all(
        [examData, statsData].map(el => Promise.all(el))
    );
    const elFiles = resolved[0]
        .map(el => JSON.parse(el))
        .map((el, i) => {
            el.stats = resolved[1][i];
            el.filename = filenames[i];
            return el;
        });
    return elFiles
}

app.get('/', async (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    const files = await getFiles()
    console.log(files);
    return res.json(files);
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`));