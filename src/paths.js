const path = require('path')

const STATIC = path.join(__dirname, '..', 'static')

module.exports = {STATIC}